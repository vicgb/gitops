# Local ArgoCD with Minikube 

Este proyecto esta pensado para realizar pruebas en local de despliegues automatizados con Gitlab y ArgoCD en un cluster local con Minikube.

La idea es registrar algun servicio en el registry privado de Gitlab y que cualquier cambio en el codigo, pusheado a la rama master sea desplegado automaticamente en un cluster de Minikube con ArgoCD.

# TODO

- [X] Desplegar ArgoCD en local
- [ ] gitlab-ci para despliegue automatico
- [ ] Despliegue de servicios 

